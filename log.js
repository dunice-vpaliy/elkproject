var winston = require('winston');
require('winston-logstash');

module.exports = function(module) {
    return makeLogger(module.filename);
};

function makeLogger(path) {
    // If you need to have settings for each module separately, you can use the commented out construction
    // if (path.match(/test.js$/)) {
    var module_name = path.split('/');
    module_name = module_name[module_name.length - 1];
    var transports = [

        new winston.transports.Console({
            timestamp: true,
            colorize: true,
            level: 'info'
        }),

        new winston.transports.Logstash({
            port: 5000,
            node_name: module_name,
            host: '127.0.0.1',
            level: 'debug',
        }),

        new winston.transports.File({
            filename: 'debug.log',
            level: 'debug'
        })
    ];

    return new winston.Logger({
        transports: transports,
        rewriters: [function(level, msg, meta) {
            host_name = 'test_host_name';
            meta = host_name
            return meta
        }]
    });
    // } else {
    //     return new winston.Logger({
    //         transport: []
    //     });
    // }
}