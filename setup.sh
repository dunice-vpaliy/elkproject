#!/bin/bash

npm install winston
npm install winston-logstash
cp logstash.conf ./docker-elk/logstash/pipeline/
cp docker.conf ./docker-elk/docker-compose.yml
cd docker-elk
docker-compose up

exit 0