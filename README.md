# Как запустить проект #

* git clone --recursive https://dunice-vpaliy@bitbucket.org/dunice-vpaliy/elkproject.git
* cd elkproject
* ./setup.sh
* Kibana грузится достаточно долго
* http://localhost:5601/app/kibana

# Как работать #

* создать индекс (простейший это *)
* зайти в вкладку Discover
* левое меню отображает все поля, над ним кнопка "add a filter" которой можно добавлять фильтры отображения данных