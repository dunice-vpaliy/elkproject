var winston = require('./log')(module);

setInterval(function() {
    winston.info("Info message");
    winston.error("Error message");
    winston.debug('Debug message');
}, 2000);