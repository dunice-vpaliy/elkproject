var url = require('url');
var log = require('./log')(module);

module.exports = function(req, res) {
    var urlParse = url.parse(req.url, true);

    log.info('Got request', req.method, req.url);

    if (req.method == 'GET' && urlParse.pathname == '/echo' && urlParse.query.message) {
        var message = urlParse.query.message;
        log.debug("Echo: " + message);
        res.end(message);
        return
    }

    log.error('Unknown URL');

    res.status = 404;
    res.end('Not Found');
}